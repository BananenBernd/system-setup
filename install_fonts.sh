FONTDIR="$HOME/.local/share/fonts/"
mkdir -p "$FONTDIR"
TMP_FONTDIR="/tmp/temp_fonts/"


## Yosemite SF Font
git clone https://github.com/supermarin/YosemiteSanFranciscoFont.git /tmp/SF
mv /tmp/SF/*.ttf "$TMP_FONTDIR"

## Anonymous Pro
#wget https://www.marksimonson.com/assets/content/fonts/AnonymousPro-1.002.zip -O /tmp/anonymous-pro.zip
wget https://www.fontsquirrel.com/fonts/download/Anonymous-Pro -O /tmp/anonymous-pro.zip
unzip /tmp/anonymous-pro.zip -d "$TMP_FONTDIR"

## Exo2
wget https://www.ndiscover.com/wp-content/themes/barrel-child/fonts/demo/exo2/Exo2.zip -O /tmp/exo-2.zip
unzip /tmp/exo-2.zip -d "$TMP_FONTDIR"

## Maven Pro
wget https://www.fontsquirrel.com/fonts/download/maven-pro -O /tmp/maven-pro.zip
unzip /tmp/maven-pro.zip -d "$TMP_FONTDIR"

## Orbitron
#wget https://github.com/theleagueof/orbitron/archive/master.zip -O /tmp/orbitron.zip
#unzip /tmp/orbitron.zip -d /tmp/
git clone https://github.com/theleagueof/orbitron /tmp/orbitron
mv /tmp/orbitron/*.ttf "$TMP_FONTDIR"

## PT Sans

## Quicksand

## Rajdhani
git clone https://github.com/itfoundry/rajdhani /tmp/rajdhani
mv /tmp/rajdhani/build/*.otf "$TMP_FONTDIR"

## Source Code Pro


## Source Sans
git clone https://github.com/adobe-fonts/source-sans.git /tmp/source_sans
mv /tmp/source_sans/OTF/*.otf "$TMP_FONTDIR"


## Alegreya
#git clone https://github.com/huertatipografica/Alegreya.git /tmp/alegreya
wget https://www.huertatipografica.com/free_download/132 -O /tmp/alegreya.zip
unzip /tmp/alegreya.zip -d "$TMP_FONTDIR"
git clone https://github.com/huertatipografica/Alegreya-Sans.git /tmp/alegreya-sans
cp /tmp/alegreya-sans/fonts/otf/*.otf "$TMP_FONTDIR"

## Anka Coder
git clone https://github.com/loafer-mka/anka-coder-fonts.git /tmp/anka-coder
mv /tmp/anka-coder/TTFs/*.ttf "$TMP_FONTDIR"

## PolarSys B612
git clone https://github.com/polarsys/b612.git /tmp/b612
mv /tmp/b612/fonts/ttf/*.ttf "$TMP_FONTDIR"

## IBM Plex
git clone https://github.com/IBM/plex.git /tmp/plex
mv /tmp/plex/IBM-Plex-*/fonts/complete/woff2/*.woff2 "$TMP_FONTDIR"


## Font Awesome 5
wget https://use.fontawesome.com/releases/v5.15.4/fontawesome-free-5.15.4-desktop.zip -O /tmp/fa5.zip
unzip /tmp/fa5.zip -d /tmp
cp "/tmp/fontawesome-free-5.15.4-desktop/ofts/*.otf" "$TMP_FONTDIR"

## Marlowe
wget https://www.dafontfree.co/wp-content/uploads/2022/06/Marlowe-Font.zip -O /tmp/marlowe.zip
unzip /tmp/marlowe.zip -d /tmp/marlowe
mv /tmp/marlowe/*.otf "$TMP_FONTDIR"


## Gill Sans
wget https://www.cufonfonts.com/get/font/download/e5ccb8bb01f54861708e03d1865561ea -O /tmp/gill-sans.zip
unzip /tmp/gill-sans.zip -d /tmp/gill-sans
mv /tmp/gill-sans/*.otf "$TMP_FONTDIR"


## Quadraat Sans
wget https://dwl.freefontsfamily.com/download/quadraat-sans-font/?wpdmdl=37525&refresh=64f602c2b933d1693844162 -O /tmp/quadraat.zip
unzip /tmp/quadraat.zip -d /tmp/quadraat
mv "/tmp/quadraat/Quadraat Sans Font/*.ttf" "$TMP_FONTDIR"


## Nohemi Typeface
wget https://app.gumroad.com/r/cbd8bb2cc69b78d091e79ead0d46db30/product_files?product_file_ids%5B%5D=uavrs6u2GlGa1P3I1t0h2g%3D%3D -O /tmp/nohemi.zip
unzip /tmp/nohemi.zip -d /tmp/nohemi
mv "/tmp/nohemi/Nohemi/OpenType-PS/*.otf" "$TMP_FONTDIR"


## Cascadia Code
wget https://github.com/microsoft/cascadia-code/releases/download/v2404.23/CascadiaCode-2404.23.zip -O /tmp/cascadia.zip
unzip /tmp/cascadia.zip -d /tmp/cascadia
mv "/tmp/cascadia/*.ttf" "$TMP_FONTDIR"


## Vollkorn
wget http://vollkorn-typeface.com/download/vollkorn-4-105.zip -O /tmp/vollkorn.zip
unzip /tmp/vollkorn.zip -d /tmp/vollkorn
mv "/tmp/vollkorn/PS-OTF/*.otf" "$TMP_FONTDIR"


## Noto Color Emoji
wget https://github.com/googlefonts/noto-emoji/raw/main/fonts/NotoColorEmoji.ttf -O /tmp/NotoColorEmoji.ttf
mv "/tmp/NotoColorEmoji.ttf" "$TMP_FONTDIR"

## JetBrains Mono
wget https://download.jetbrains.com/fonts/JetBrainsMono-1.0.3.zip -O /tmp/jetbrainsmono.zip
unzip /tmp/jetbrainsmono.zip -d /tmp/jetbrainsmono
mv "/tmp/jetbrainsmono/JetBrainsMono-1.0.3/ttf/*.ttf" "$TMP_FONTDIR"


## Metropolis
wget https://fontsarena.com/wp-content/uploads/2018/11/Metropolis-r11.zip -O /tmp/metropolis.zip
unzip /tmp/metropolis.zip -d /tmp/metropolis
mv "/tmp/metropolis/Metropolis-r11/Fonts/OpenType/*otf" "$TMP_FONTDIR"


# Copy all font files
cp "$TMP_FONTDIR/"*.otf $FONTDIR
cp "$TMP_FONTDIR/"*.ttf $FONTDIR
