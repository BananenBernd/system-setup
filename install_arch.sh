#!/bin/bash
# Install important packages
filename="arch_packages.txt"
while read -r line
do
	sudo pacman -S --noconfirm "$line"
done < "$filename"


# Install additional fonts
./install_fonts.sh

# Install fancy i3lock
git clone https://github.com/meskarune/i3lock-fancy.git /tmp/i3lock-fancy
cp /tmp/i3lock-fancy/i3lock-fancy $HOME/.local/bin
cp -R /tmp/i3lock-fancy/icons $HOME/.local/bin
echo "You have to adjust the icon paths in i3lock-fancy to ~/.local/bin"
#sudo cp /tmp/i3lock-fancy/lock /usr/local/bin
#sudo cp -R /tmp/i3lock-fancy/icons /usr/local/bin

# Install dot files
#git clone https://gitlab.com/BananenBernd/config-files.git /tmp/dotfiles
#cp -R /tmp/dotfiles* -t ~
